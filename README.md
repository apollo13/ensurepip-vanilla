Ensurepip-Vanilla
=================

`ensurepip-vanilla` is copy of the upstream `ensurepip` with an up2date `pip` & `wheel`. It is ment to be installed on Debian/Ubuntu instead of `pythonXX-venv`. This way, virtualenvs get a usable `pip`.

Installation
------------

It is very important that this package is installed into the global python lib directory and not via `site-packages`. The reasoning is quite simple, as the `venv` module calls:

```python
# We run ensurepip in isolated mode to avoid side effects from
# environment vars, the current directory and anything else
# intended for the global Python environment
cmd = [context.env_exe, '-Im', 'ensurepip', '--upgrade',
                                                '--default-pip']
subprocess.check_output(cmd, stderr=subprocess.STDOUT)
```

So to install, we need (as root & adjust for your python version):

```
pip install --target /usr/lib/python3.6 ensurepip-vanilla
```

After installation all your virtualenvs should have a resonably new `pip`.
